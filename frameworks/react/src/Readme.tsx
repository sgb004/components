import Examples from './Examples';

const Readme = () => (
	<div id="readme">
		<Examples />
	</div>
);

export default Readme;
