const fs = require('fs');
const showdown = require('showdown');
const cheerio = require('cheerio');

const getReadme = (err, readmeData) => {
	if (err) throw err;

	const converter = new showdown.Converter({ noHeaderId: true });
	let readmeHtml = converter.makeHtml(readmeData);
	readmeHtml = cleanReadmeHtml(readmeHtml);

	writeReadmeJsx(readmeHtml);
	writeIndexHtml(readmeHtml);
};

const cleanReadmeHtml = (readmeHtml) => {
	readmeHtml = readmeHtml.replace(/\$/g, '&#36;');
	readmeHtml = readmeHtml.replace(/{/g, '&#123;');
	readmeHtml = readmeHtml.replace(/}/g, '&#125;');

	readmeHtml = readmeHtml.replace(/<code(.*?)>(.*?)<\/code>/gs, (match, attrs, codeContent) => {
		const codeContentWithBr = codeContent.replace(/\n/g, '<br/>');
		return `<code>${codeContentWithBr}</code>`;
	});

	return readmeHtml;
};

const writeReadmeJsx = (readmeHtml) => {
	readmeHtml = readmeHtml.replace(/<!-- Examples -->/g, '<Examples />');
	readmeHtml = readmeHtml.replace(/<!--/g, '{/*');
	readmeHtml = readmeHtml.replace(/-->/g, '*/}');

	readmeHtml = `import Examples from './Examples';

const Readme = () => (
	<div id="readme">
	${readmeHtml}
	</div>
);

export default Readme;
	`;

	fs.writeFile('./src/Readme.tsx', readmeHtml, (err) => {
		if (err) throw err;
		console.log('README.md copied to src/Readme.tsx');
	});
};

const writeIndexHtml = (readmeHtml) => {
	fs.readFile('./dev/index.html', 'utf8', (err, data) => {
		if (err) throw err;

		const $ = cheerio.load(data);
		const examples = $('#examples');
		let readme = $('#readme');

		if (readme.length === 0) {
			$('body').prepend('<div id="readme"></div>');
			readme = $('#readme');
		}

		readmeHtml = readmeHtml.replace(
			/<!-- Examples -->/g,
			`<div id="examples">${examples.html()}</div>`
		);

		examples.remove();
		readme.html(readmeHtml);

		fs.writeFile('./dev/index.html', $.html(), (err) => {
			if (err) throw err;
			console.log('README.md copied to dev/index.html');
		});
	});
};

fs.readFile('./README.md', 'utf8', getReadme);
