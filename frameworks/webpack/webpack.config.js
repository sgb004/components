const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const path = require('path');

module.exports = (env, argv) => {
	let entry = {
		'[component-name-hyphens]': [
			path.resolve(__dirname, 'src') + '/[component-name-hyphens].ts',
			/* component-css */
		],
	};

	if (env.WEBPACK_SERVE) {
		entry['index'] = [path.resolve(__dirname, 'src') + '/index.css'];
	}

	return {
		entry,
		output: {
			path: path.resolve(__dirname, 'dist'),
			filename: '[name].js',
		},
		plugins: [new MiniCSSExtractPlugin()],
		module: {
			rules: [
				{
					test: /\.ts$/,
					use: [
						{
							loader: 'ts-loader',
							options: {
								configFile: 'tsconfig.webpack.json',
							},
						},
					],
					exclude: [/node_modules/],
				},
				{
					test: /\.css$/,
					use: [
						MiniCSSExtractPlugin.loader,
						'css-loader',
						{
							loader: 'string-replace-loader',
							options: {
								multiple: [
									//Debido al uso de :host en los webcomponents es posible que se requieran reemplazar algunas clases
									//{ search: reg-exp, replace: texto }
								],
							},
						},
					],
				},
			],
		},
		resolve: {
			extensions: ['.js', '.ts', '.css'],
		},
		devtool: false,
		optimization: {
			minimizer: ['...', new CssMinimizerPlugin()],
		},
		devServer: {
			static: {
				directory: path.join(__dirname, 'dev'),
			},
			compress: true,
			port: 9000,
		},
	};
};
