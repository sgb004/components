const { exec } = require('child_process');
const { exit } = require('process');
const readline = require('readline');
const fs = require('fs');
const path = require('path');
const spinner = require('simple-spinner');

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
});

let componentNameHyphens = path.basename(process.cwd());
let componentName = componentNameHyphens
	.split(/\s|\_|\-/)
	.map((word) => word.charAt(0).toUpperCase() + word.slice(1))
	.join(' ');
let componentNameCamel = '';

let useCSS = false;
let useWebpack = false;
let useCopyReadme = false;
let gitRemote = '';
let autoFirstCommit = false;

/* FUNCTIONS */

const isYes = (answer) =>
	answer === 'Y' || answer === 'y' || answer === null || answer === undefined || answer === '';

const addBreakLine = () => console.log('\n');

const showSpinner = (text) =>
	spinner.start({
		sequence: 'dots',
		interval: 80,
		hideCursor: true,
		text,
	});

const addBreakLineAndShowSpinner = (text) => {
	//console.log('\r');
	showSpinner(text);
};

const upperCaseFirstLetter = (text) => text.charAt(0).toUpperCase() + text.slice(1);

const buildNames = () => {
	const componentNameLower = componentName.toLowerCase();

	componentNameCamel = componentNameLower
		.split(' ')
		.map((word) => upperCaseFirstLetter(word))
		.join('');

	componentNameHyphens = componentNameLower
		.replace(/ /g, '-')
		.replace(/\//g, '')
		.replace(/\\/g, '');
};

const execCommand = (beforeText, afterText, command) =>
	new Promise((resolve, reject) => {
		addBreakLineAndShowSpinner(beforeText);

		exec(command, (error, stdout, stderr) => {
			spinner.stop();

			if (error) {
				reject(error);
				return;
			}

			console.log(afterText);

			resolve();
		});
	});

const cleanPath = (path) => path.replace(/\.\./g, '').replace(/\/\//g, '/');

const deleteDirectory = (directory) =>
	execCommand(
		`Deleting ${directory} files dir...`,
		`${upperCaseFirstLetter(directory)} files deleted`,
		`rm -r ./${componentNameHyphens}/${cleanPath(directory)}`
	);

const copyFiles = (framework, directory) =>
	execCommand(
		`Copying ${directory} files...`,
		`${upperCaseFirstLetter(directory)} files copied`,
		`cp -r ./frameworks/${cleanPath(
			framework
		)}/${directory} ./${componentNameHyphens}/${cleanPath(directory)}`
	);

const copyFile = (framework, file, fileName) =>
	execCommand(
		`Copying ${fileName}...`,
		`${fileName} copied`,
		`cp ./frameworks/${cleanPath(framework)}/${file} ./${componentNameHyphens}/${cleanPath(
			file
		)}`
	);

const getFileContent = (path) =>
	new Promise((resolve, reject) => {
		fs.readFile(`./${cleanPath(path)}`, 'utf8', (err, data) => {
			if (err) {
				reject(err);
				return;
			}

			resolve(data);
		});
	});

const saveFileContent = (fileName, content) =>
	new Promise((resolve, reject) => {
		fs.writeFile(`./${componentNameHyphens}/${cleanPath(fileName)}`, content, (err) => {
			if (err) {
				reject(err);
				return;
			}

			resolve();
		});
	});

const editJson = (beforeText, afterText, file, changer) => {
	addBreakLineAndShowSpinner(beforeText);

	return new Promise((resolve, reject) =>
		getFileContent(`./${componentNameHyphens}/${cleanPath(file)}.json`)
			.then((fileContent) => JSON.parse(fileContent))
			.then((data) =>
				saveFileContent(
					`${cleanPath(file)}.json`,
					JSON.stringify(changer(data), null, '\t')
				)
			)
			.then(() => {
				spinner.stop();
				console.log(afterText);
				resolve();
			})
			.catch((error) => spinner.stop() && reject(error))
	);
};

const createAppReact = () =>
	execCommand(
		'Creating React App...',
		'React App created',
		`npx create-react-app ${componentNameHyphens} --template typescript`
	);

const cleanReadmeReact = () =>
	execCommand(
		'Cleaning readme...',
		'Readme cleaned',
		`echo "# ${componentName}\n\n<!-- Examples -->" > ${componentNameHyphens}/README.md`
	);

const addIndexHtmlReact = () =>
	execCommand(
		'Adding index.html',
		'index.html added',
		`mkdir ./${componentNameHyphens}/public && echo "<!DOCTYPE html>\n<html lang=\\"en\\">\n\t<head>\n\t\t<meta charset=\\"utf-8\\" />\n\t\t<meta name=\\"viewport\\" content=\\"width=device-width, initial-scale=1\\" />\n\t\t<meta name=\\"theme-color\\" content=\\"#000000\\" />\n\t\t<title>${componentName}</title>\n\t</head>\n\t<body>\n\t\t<noscript>You need to enable JavaScript to run this app.</noscript>\n\t\t<div id=\\"root\\"></div>\n\t</body>\n</html>" > ./${componentNameHyphens}/public/index.html`
	);

const addMainComponentReact = () =>
	execCommand(
		'Adding main component...',
		'Main component added',
		`echo "import React from 'react';\n\nimport './${componentNameHyphens}.css';\n\nconst ${componentNameCamel} = () => <>${componentName}</>;\n\nexport default ${componentNameCamel};" > ${componentNameHyphens}/src/${componentNameCamel}.tsx`
	);

const editGitignoreReact = () =>
	execCommand(
		'Editing .gitignore...',
		'.gitignore edited',
		`echo "\n/_ed" >> ${componentNameHyphens}/.gitignore`
	);

const installModulesWebpack = () =>
	execCommand(
		'Installing modules...',
		'Modules installed',
		`cd ${componentNameHyphens} && npm i @babel/cli @babel/core @babel/preset-react concurrently css-loader css-minimizer-webpack-plugin mini-css-extract-plugin string-replace-loader ts-loader webpack webpack-cli webpack-dev-server -D`
	);

const addIndexHtmlWebpack = () =>
	execCommand(
		'Adding index.html',
		'index.html added',
		`mkdir ./${componentNameHyphens}/dev && echo "<!DOCTYPE html>\n<html lang=\\"en\\">\n\t<head>\n\t\t<meta charset=\\"utf-8\\" />\n\t\t<meta name=\\"viewport\\" content=\\"width=device-width, initial-scale=1\\" />\n\t\t<meta name=\\"theme-color\\" content=\\"#000000\\" />\n\t\t<title>${componentName}</title>\n\t\t<link rel=\\"stylesheet\\" href=\\"./index.css\\">${
			useCSS
				? `\n\t\t<link rel=\\"stylesheet\\" href=\\"./${componentNameHyphens}.css\\">`
				: ''
		}\n\t</head>\n\t<body>\n\t\t<div id=\\"examples\\"><${componentNameHyphens} /></div>\n\t\t<script src=\\"./${componentNameHyphens}.js\\"></script>\n\t</body>\n</html>" > ./${componentNameHyphens}/dev/index.html`
	);

const copyWebpackConfig = () => {
	addBreakLineAndShowSpinner('Copying webpack.config.js...');

	return new Promise((resolve, reject) =>
		getFileContent('./frameworks/webpack/webpack.config.js')
			.then((fileContent) => {
				fileContent = fileContent.replace(
					/\[component-name-hyphens\]/g,
					componentNameHyphens
				);
				fileContent = fileContent.replace(
					'/* component-css */',
					useCSS ? `path.resolve(__dirname, 'src') + '/${componentNameHyphens}.css',` : ''
				);

				return saveFileContent('webpack.config.js', fileContent);
			})
			.then(() => {
				spinner.stop();
				console.log('webpack.config.js copied');
				resolve();
			})
			.catch((error) => spinner.stop() && reject(error))
	);
};

const addExcludeToTsconfigWebpack = () =>
	editJson(
		'Add exclude to tsconfig.json...',
		'Exclude added to tsconfig.json',
		'tsconfig',
		(data) => {
			const excludeFile = `src/${componentNameHyphens}.ts`;

			if (data['exclude']) {
				if (data['exclude'].indexOf(excludeFile) === -1) data['exclude'].push(excludeFile);
			} else {
				data['exclude'] = [excludeFile];
			}

			return data;
		}
	);

const changeScriptsInPackageJsonWebpack = () =>
	editJson(
		'Changing scripts in package.json...',
		'Scripts changed in package.json',
		'package',
		(data) => {
			data['scripts'] = {
				start: 'concurrently "npm run start_react" "npm run start_webpack"',
				build: 'concurrently "npm run build_react" "npm run build_webpack"',
				start_react: 'react-scripts start',
				build_react: 'react-scripts build',
				test_react: 'react-scripts test',
				eject_react: 'react-scripts eject',
				start_webpack: 'npx webpack serve --config webpack.config.js --mode development',
				start_webpack_prod:
					'npx webpack serve --config webpack.config.js --mode production',
				build_webpack: 'npx webpack --config webpack.config.js --mode production',
				build_webpack_devel: 'npx webpack --config webpack.config.js --mode development',
				build_dist: `npm run build_react && npm run build_webpack && npx tsc --jsx preserve -t es2016 --outDir dist/react --noEmit false && npx babel dist/react/${componentNameCamel}.jsx -d dist/react/ && rm dist/react/*.jsx${
					useCSS
						? ` && cp src/${componentNameHyphens}.css dist/react/${componentNameHyphens}.css`
						: ''
				}`,
			};
			return data;
		}
	);

const addExamplesComponentReact = () =>
	execCommand(
		'Adding examples component...',
		'Examples component added',
		`echo "import React from 'react';\nimport ${componentNameCamel} from './${componentNameCamel}';\n\nconst Examples = () => { return <div id=\\"examples\\"><${componentNameCamel} /></div>; };\n\nexport default Examples;" > ./${componentNameHyphens}/src/Examples.tsx`
	);

const addMainComponentWebpack = () =>
	execCommand(
		'Adding main component...',
		'Main component added',
		`echo "class ${componentNameCamel} extends HTMLElement {\n\n\tconstructor() {\n\t\tsuper();\n\n\t\tconst shadow: ShadowRoot = this.attachShadow({ mode: 'open' });\n\t\tconst div: HTMLDivElement = document.createElement('div');\n\n\t\tdiv.textContent = '${componentName}';\n\t\tshadow.appendChild(div);\n\t}\n\n\tconnectedCallback() {}\n\n\tdisconnectedCallback() {}\n}\n\ncustomElements.define('${componentNameHyphens}', ${componentNameCamel});" > ${componentNameHyphens}/src/${componentNameHyphens}.ts`
	);

const addCSSComponentWebpack = () =>
	useCSS
		? execCommand(
				'Adding CSS to the component...',
				'Added CSS to the component ',
				`echo "" > ${componentNameHyphens}/src/${componentNameHyphens}.css`
		  )
		: new Promise((resolve) => resolve());

const editGitignoreWebpack = () =>
	execCommand(
		'Editing .gitignore...',
		'.gitignore edited',
		`echo "\n/dev/*\n!/dev/index.html" >> ${componentNameHyphens}/.gitignore`
	);

const installModulesCopyReadme = () =>
	execCommand(
		'Installing modules...',
		'Modules installed',
		`cd ${componentNameHyphens} && npm i cheerio showdown -D`
	);

const changeScriptsInPackageJsonCopyReadme = () =>
	editJson(
		'Changing scripts in package.json...',
		'Scripts changed in package.json',
		'package',
		(data) => {
			data['scripts'] = {
				...data['scripts'],
				copy_readme: 'node copy-readme.js',
			};

			if (data['scripts']['build_dist']) {
				data['scripts'][
					'build_dist'
				] = `npm run copy_readme && ${data['scripts']['build_dist']}`;
			}

			return data;
		}
	);

const deleteGit = () => execCommand('Deleting git...', 'Git deleted', `rm -rf ./.git`);

const addGit = () =>
	execCommand('Adding git...', 'Git added', `cd ${componentNameHyphens} && git init`);

const addRemoteOriginGit = () =>
	execCommand(
		'Adding remote origin...',
		'Remote origin added',
		`cd ${componentNameHyphens} && git remote add origin ${gitRemote}`
	);

const commitGit = () =>
	execCommand(
		'Commiting...',
		'Commited',
		`cd ${componentNameHyphens} && git add . && git commit -m "Initial commit"`
	);

const deleteFilesCore = () =>
	execCommand(
		'Deleting core files...',
		'Core files deleted',
		`if [ -d ".git" ]; then rm -r .git; fi && 
	rm -r node_modules &&
	rm -r frameworks &&
	rm -r index.js && 
	rm -r package.json && 
	rm -r package-lock.json && 
	rm -r README.md &&
	rm .gitignore `
	);

const copyComponentFiles = () => {
	addBreakLineAndShowSpinner('Copying component files to parent directory...');

	return new Promise((result, reject) => {
		fs.readdir(`./${componentNameHyphens}`, (error, files) => {
			spinner.stop();

			if (error) throw reject(error);

			files.forEach((file) => {
				const oldPath = `./${componentNameHyphens}/${file}`;
				const newPath = `./${file}`;

				fs.rename(oldPath, newPath, (error) => {
					if (error) throw reject(error);
				});
			});

			console.log('Copied component files to parent directory');
			result();
		});
	});
};

const deleteComponentDirectory = () =>
	execCommand(
		'Deleting component directory...',
		'Component directory deleted',
		`rm -r ./${componentNameHyphens}`
	);

/* FRAMEWORKS */

const react = () => {
	addBreakLine();
	console.log('---- REACT.JS ----');

	return new Promise((resolve, reject) =>
		createAppReact()
			.then(() => cleanReadmeReact())
			.then(() => deleteDirectory('public'))
			.then(() => addIndexHtmlReact())
			.then(() => deleteDirectory('src'))
			.then(() => copyFiles('react', 'src'))
			.then(() => addExamplesComponentReact())
			.then(() => addMainComponentReact())
			.then(() => editGitignoreReact())
			.then(() => resolve())
			.catch(reject)
	);
};

const webpack = () => {
	addBreakLine();
	console.log('---- WEBPACK ----');

	return new Promise((resolve, reject) =>
		useWebpack
			? installModulesWebpack()
					.then(() => addIndexHtmlWebpack())
					.then(() => copyFile('webpack', '.babelrc', '.babelrc'))
					.then(() =>
						copyFile('webpack', 'tsconfig.webpack.json', 'tsconfig.webpack.json')
					)
					.then(() => copyWebpackConfig())
					.then(() => addExcludeToTsconfigWebpack())
					.then(() => changeScriptsInPackageJsonWebpack())
					.then(() => addMainComponentWebpack())
					.then(() => addCSSComponentWebpack())
					.then(() => editGitignoreWebpack())
					.then(() => resolve())
					.catch(reject)
			: resolve()
	);
};

const copyReadme = () => {
	addBreakLine();
	console.log('---- COPY README ----');

	return new Promise((resolve, reject) =>
		useCopyReadme
			? installModulesCopyReadme()
					.then(() => copyFile('copy-readme', 'copy-readme.js', 'copy-readme.js'))
					.then(() => changeScriptsInPackageJsonCopyReadme())
					.then(() => resolve())
					.catch(reject)
			: resolve()
	);
};

const deleteCore = () => {
	addBreakLine();
	console.log('---- DELETE CORE ----');

	return new Promise((resolve, reject) =>
		deleteFilesCore()
			.then(() => copyComponentFiles())
			.then(() => deleteComponentDirectory())
			.then(() => resolve())
			.catch(reject)
	);
};

/* GIT */

const git = () => {
	addBreakLine();
	console.log('---- GIT ----');

	return new Promise((resolve, reject) =>
		gitRemote === ''
			? resolve()
			: deleteGit()
					.then(() => addGit())
					.then(() => addRemoteOriginGit())
					.then(() => (autoFirstCommit ? commitGit() : resolve()))
					.then(() => resolve())
					.catch(reject)
	);
};

/* QUESTIONS */
const questionName = () =>
	new Promise((resolve) =>
		rl.question(`What is the name of the component? (${componentName}) `, (answer) =>
			resolve(answer)
		)
	);

const questionCSS = () =>
	new Promise((resolve) =>
		rl.question('Do you want to add CSS? [Y/N] ', (answer) => resolve(answer))
	);

const questionWebpack = () =>
	new Promise((resolve) =>
		rl.question('Do you want to add webpack? [Y/N] ', (answer) => resolve(answer))
	);

const questionCopyReadme = () =>
	new Promise((resolve) =>
		rl.question('Do you want to use copy readme? [Y/N] ', (answer) => resolve(answer))
	);

const questionGit = () =>
	new Promise((resolve) =>
		rl.question('Do you want to add a remote git? ', (answer) => resolve(answer))
	);

const questionAutoFirstCommit = () =>
	new Promise((resolve) =>
		rl.question(
			'Do you want the first commit to be automated after the changes? [Y/N] ',
			(answer) => resolve(answer)
		)
	);

/* RUN */
questionName()
	.then((answer) => {
		if (answer !== '') componentName = answer;
		return questionCSS();
	})
	.then((answer) => {
		if (isYes(answer)) useCSS = true;
		return questionWebpack();
	})
	.then((answer) => {
		if (isYes(answer)) useWebpack = true;
		return questionCopyReadme();
	})
	.then((answer) => {
		if (isYes(answer)) useCopyReadme = true;
		return questionGit();
	})
	.then((answer) => {
		gitRemote = answer;
		return gitRemote === '' ? false : questionAutoFirstCommit();
	})
	.then((answer) => {
		if (isYes(answer)) autoFirstCommit = true;

		buildNames();

		return react();
	})
	.then(() => webpack())
	.then(() => copyReadme())
	.then(() => git())
	.then(() => deleteCore())
	.then(() => {
		addBreakLine();
		console.log('---- READY TO CODE ----');
		addBreakLine();
		rl.close();
		exit();
	})
	.catch((error) => console.log('ERROR', error) && rl.close() && exit());
